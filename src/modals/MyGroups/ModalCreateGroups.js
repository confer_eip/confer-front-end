import React, { Component } from 'react';
import './../../Components/Profile Groups/profileGroups.css';
import axios from 'axios';
import { Modal, Button } from 'react-bootstrap';
import { message } from 'antd';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('../../config.json');

class ModalCreateGroups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      group_name: ""
    };

    this.handleClickGroup = this.handleClickGroup.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {

    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleClickGroup() {
    if (this.state.group_name === "")
      message.error("Veuillez rentrer un nom de groupe")
    else {
      var token = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/group/create",
        data: {
          "group_name": this.state.group_name,
          "jwt": token.jwt
        }
      })
      .then(res => {
        this.props.onExitModal();
        message.success("Nouveau groupe ajouté avec succès");
      })
      .catch(function (error) {
        message.error("Echec de la création de groupe");
      });
    }
  }

  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{textAlign: "center"}} centered>
        <Modal.Header closeButton>
          <Modal.Title>Créer un groupe de JDR</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input type="text" placeholder="Nom du groupe" value={this.state.group_name} onChange={event => this.setState({group_name:event.target.value})}/>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Annuler
          </Button>
          <Button variant="primary" onClick={this.handleClickGroup}>
            Créer le groupe
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalCreateGroups;
