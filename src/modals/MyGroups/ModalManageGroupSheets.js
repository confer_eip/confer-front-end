import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { message } from 'antd';
import axios from 'axios';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('./../../config.json');

class ModalShareSheetWGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sheet_id_share: "",
      sheet_id_unshare: ""
    };

    this.handleShareSheet = this.handleShareSheet.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleShareSheet = () => {
    if (this.state.sheet_id_share === "")
      message.error("Veuillez entrer l'id de la feuille de personnage");
    else {
      var token = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/group/share_sheet",
        data: {
          "jwt": token.jwt,
          "sheet_id": parseInt(this.state.sheet_id_share, 10),
          "group_id": parseInt(this.props.group_id, 10)
        }
      })
      .then(res => {
        this.props.onExitModal();
        message.success("La feuille a été partagée");
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
    }
  }

  handleUnshareSheet = () => {
    if (this.state.sheet_id_unshare === "")
      message.error("Veuillez entrer l'id d'une feuille de personnage");
    else {
      var token = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/group/unshare_sheet",
        data: {
          "jwt": token.jwt,
          "sheet_id": parseInt(this.state.sheet_id_unshare, 10),
          "group_id": parseInt(this.props.group_id, 10)
        }
      })
      .then(res => {
        this.props.onExitModal();
        message.success("La feuille n'est plus partagée");
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
    }
  }

  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{textAlign: "center"}} centered>
        <Modal.Header closeButton>
          <Modal.Title>Gérer les feuilles de personnages du groupe</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>Pour partager une feuille avec un groupe,<br />veuillez rentrer l'id de la feuille ci-dessous :</h6>
          <input type="text" id="d" name="sheet_id_share" placeholder="Id de la feuille" value={this.state.sheet_id_share} onChange={event => this.setState({ sheet_id_share: event.target.value })} />
          <Button variant="primary" onClick={this.handleShareSheet}>Partager une feuille de personnage</Button>
          <br /><br />
          <h6>Pour ne plus partager une feuille avec un groupe,<br /> veuillez rentrer l'id de la feuille ci-dessous :</h6>
          <input type="text" id="d" name="sheet_id_unshare" placeholder="Id de la feuille" value={this.state.sheet_id_unshare} onChange={event => this.setState({ sheet_id_unshare: event.target.value })} />
          <Button variant="primary" onClick={this.handleUnshareSheet}>Ne plus partager la feuille</Button>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Fermer
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalShareSheetWGroup;
