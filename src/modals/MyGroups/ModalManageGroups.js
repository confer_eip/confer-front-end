import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import axios from 'axios';
import './../../Components/Profile Groups/profileGroups.css';
import Cookies from 'universal-cookie';
import { message } from 'antd';
export const cookies = new Cookies();
var api_url = require('../../config.json');

class ModalManageGroups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: "",
      username: "",
      owner_id: "",
      invitation_id: "",
      new_owner_id: ""
    };

    this.handleChange = this.handleChange;
    this.addUser = this.addUser.bind(this);
    this.leaveGroup = this.leaveGroup.bind(this);
    this.deleteGroup = this.deleteGroup.bind(this);
    this.cancelInvitation = this.cancelInvitation.bind(this);
    this.changeOwner = this.changeOwner.bind(this);
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  leaveGroup() {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/group/leave",
      data: {
        "group_id": this.props.group_infos.group_id,
        "jwt": token.jwt
      }
    })
      .then(res => {
        this.props.onExitModal();
        message.success("Vous avez quitté le groupe avec succès");
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
        message.info("Seriez-vous le propriétaire du groupe ?");
      });
  }

  deleteGroup() {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/group/remove",
      data: {
        "group_id": this.props.group_infos.group_id,
        "group_name": this.props.group_infos.group_name,
        "jwt": token.jwt
      }
    })
      .then(res => {
        this.props.onExitModal();
        message.success("Le groupe a été supprimé avec succès");
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
  }

  cancelInvitation() {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/group/cancel_invite",
      data: {
        "group_id": parseInt(this.props.group_infos.group_id),
        "user_id": parseInt(this.state.invitation_id),
        "jwt": token.jwt
      }
    })
      .then(res => {
        this.props.onExitModal();
        message.success("L'invitation a bien été annulé");
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
  }

  changeOwner() {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/group/change_owner",
      data: {
        "group_id": parseInt(this.props.group_infos.group_id),
        "user_id": parseInt(this.state.owner_id),
        "jwt": token.jwt
      }
    })
      .then(res => {
        this.props.onExitModal();
        message.success("Le propriétaire a été changé");
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
  }

  addUser = () => {
    if (this.state.user_id === "" && this.state.username === "")
      message.error("Veuillez rentrer un ID ou un Nom d'utilisateur")
    else {
      var token = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/group/invite",
        data: {
          "group_id": parseInt(this.props.group_infos.group_id, 10),
          "user_id": parseInt(this.state.user_id, 10),
          "user_name": this.state.username,
          "jwt": token.jwt
        }
      })
        .then(res => {
          this.props.onExitModal();
          message.success("L'invitation au groupe a été envoyée à l'utilisateur");
        })
        .catch(function (error) {
          message.error("Une erreur est survenue, veuillez réessayer");
        });
    }
  }

  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{ textAlign: "center" }} centered>
        <Modal.Header closeButton>
          <Modal.Title>Groupe {this.props.group_infos.group_name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.props.group_infos.group_owner_id === this.props.user_id ?
            <div>
              <form>
                <input type="text" placeholder="ID de l'utilisateur à ajouter"
                  value={this.state.user_id} onChange={event => this.setState({ user_id: event.target.value })} />
                <input type="text" placeholder="Nom de l'utilisateur à ajouter"
                  value={this.state.username} onChange={event => this.setState({ username: event.target.value })} />
                <Button variant="primary" onClick={this.addUser}>Ajouter membre</Button>
              </form>
              <br />
              <form>
                <input type="text" placeholder="ID de l'utilisateur invité"
                  value={this.state.invitation_id} onChange={event => this.setState({ invitation_id: event.target.value })} />
                <Button variant="primary" onClick={this.cancelInvitation}>Annuler l'invitation</Button>
              </form >
              <br />
              <form style={{ marginBottom: "50px" }}>
                <input type="text" placeholder="ID du nouveau propriétaire"
                  value={this.state.owner_id} onChange={event => this.setState({ owner_id: event.target.value })} />
                <Button variant="primary" onClick={this.changeOwner}>Changer de propriétaire</Button>
              </form>
              <Button variant="danger" onClick={this.deleteGroup}>
                Supprimer le groupe
              </Button>
            </div>
            : null}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Fermer
          </Button>
          <Button variant="warning" onClick={this.leaveGroup}>
            Quitter le groupe
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalManageGroups;
