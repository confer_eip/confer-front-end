import React, { Component } from 'react';
import { Modal, Button, OverlayTrigger, Popover } from 'react-bootstrap';
import { message } from 'antd';
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import axios from 'axios';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('./../config.json');

class ModalBlockGallery extends Component {
  voteTemplate = (rating) => {
    var token = cookies.get('LoginToken');
    if (!token)
      message.error("Veuillez vous connecter pour pouvoir noter ce modèle");
    else {
      axios({
        method: "post",
        url: api_url.api_url + "/api/gallery/vote",
        data: {
          "template_id": this.props.sheet.template_id,
          "score": rating,
          "jwt": token.jwt
        }
      })
        .then(({ data }) => {
          message.success("La note a bien été attribuée");
          window.location.reload();
        })
        .catch(function (error) {
          message.error("Une erreur est survenue, veuillez réessayer");
        });
    }
  }

  useTemplate = () => {
    var token = cookies.get('LoginToken');
    if (!token) {
      message.error("Veuillez vous connecter pour pouvoir récupérer ce modèle");
    }
    else {
      axios({
        method: "post",
        url: api_url.api_url + "/api/sheet/create-from-template",
        data: {
          "template_id": this.props.sheet.template_id,
          "jwt": token.jwt
        }
      })
        .then(({ data }) => {
          message.success("La feuille de personnage a été ajoutée dans votre profil", 2).then(window.location.reload());

        })
        .catch(function (error) {
          message.error("Une erreur est survenue, veuillez réessayer");
        });
    }
  }

  previewTemplate = () => {
    var token = cookies.get('LoginToken');
    if (!token) {
      message.error("Veuillez vous connecter pour pouvoir récupérer ce modèle");
    }
    else {
      axios({
        method: "post",
        url: api_url.api_url + "/api/gallery/get_template",
        data: {
          "template_id": this.props.sheet.template_id,
          "jwt": token.jwt
        }
      })
        .then(res => {
          res.data.isFromGalerie = true;
          this.props.redirect({
            pathname: "/personnage",
            data: res.data,
          });
        })
        .catch(function (error) {
          message.error("Une erreur est survenue, veuillez réessayer");
        });
    }
  }

  render() {
    const popover = (
      <Popover id="popover-basic">
        <Popover.Content>
          <Rater total={5} onRate={({ rating }) => this.voteTemplate(rating)} />
        </Popover.Content>
      </Popover>
    );

    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{ textAlign: "center" }} centered>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.sheet.template_name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p><i>posté par <b>{this.props.sheet.owner_name}</b> le {this.props.sheet.publication_date}</i></p>
          <Rater total={5} rating={this.props.sheet.vote_average} interactive={false} />
          <p>{this.props.sheet.vote_number} vote(s)</p>
          <p>{this.props.sheet.template_description}</p>
          <br />
          <OverlayTrigger trigger="click" placement="left" overlay={popover}>
            <Button variant="primary">
              Noter ce modèle
            </Button>
          </OverlayTrigger>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Fermer
          </Button>
          <Button variant="secondary" onClick={this.previewTemplate}>
            Preview
          </Button>
          <Button variant="primary" onClick={this.useTemplate}>
            Récupérer ce modèle
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalBlockGallery;
