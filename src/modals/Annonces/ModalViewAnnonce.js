import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';

class ModalViewAnnonce extends Component {
  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{textAlign: "center"}} centered>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.announcement.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{this.props.announcement.created_at}</p>
          <p>{this.props.announcement.description}</p>
          <p style={{textAlign: "right"}}><i>posté par {this.props.announcement.advertiser_name}</i></p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Fermer
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalViewAnnonce;
