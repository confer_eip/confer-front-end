import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import axios from 'axios';
import { message } from 'antd';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('./../../config.json');

class ModalModifyAnnonce extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.announcement.title,
      description: this.props.announcement.description
    };

    this.handleChange = this.handleChange;
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  modifyAnnonce = () => {
    if (this.state.title === "" && this.state.description === "")
      message.error("Veuillez remplir tous les champs présents ci-dessus");
    else {
      var token = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/announcement/update",
        data: {
          "jwt": token.jwt,
          "announcement_id": this.props.announcement.id,
          "announcement_title": this.state.title,
          "announcement_description": this.state.description
        }
      })
      .then(res => {
        message.success("L'annonce a été postée");
        window.location.reload();
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
    }
  }

  deleteAnnonce = () => {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/announcement/remove",
      data: {
        "jwt": token.jwt,
        "announcement_id": this.props.announcement.id
      }
    })
    .then(res => {
      window.location.reload();
      message.success("L'annonce a été suprimée");
    })
    .catch(function (error) {
      message.error("Une erreur est survenue, veuillez réessayer");
    });
  }

  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{textAlign: "center"}} centered>
        <Modal.Header closeButton>
          <Modal.Title>Modifier l'annonce</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input type="text" name="title" placeholder={this.props.announcement.title}
            value={this.state.title}
            onChange={event => this.setState( {title:event.target.value} )} />
          <textarea rows="4" cols="50"
            placeholder={this.props.announcement.description}
            value={this.state.description}
            onChange={event => this.setState( {description:event.target.value} )}>
          </textarea>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Fermer
          </Button>
          <Button variant="danger" onClick={this.deleteAnnonce}>
            Supprimer l'annonce
          </Button>
          <Button variant="primary" onClick={this.modifyAnnonce}>
            Modifier l'annonce
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalModifyAnnonce;
