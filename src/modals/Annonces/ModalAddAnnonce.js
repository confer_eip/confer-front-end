import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import axios from 'axios';
import { message } from 'antd';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('./../../config.json');

class ModalAddAnnonce extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: ""
    };

    this.handleChange = this.handleChange;
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  addingAnnonce = () => {
    if (this.state.title === "" || this.state.description === "")
      message.error("Veuillez remplir tous les champs présents ci-dessus");
    else {
      var token = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/announcement/create",
        data: {
          "jwt": token.jwt,
          "announcement_title": this.state.title,
          "announcement_description": this.state.description
        }
      })
      .then(res => {
        this.props.onExitModal();
        window.location.reload();
        message.success("L'annonce a été postée");
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
    }
  }

  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{textAlign: "center"}} centered>
        <Modal.Header closeButton>
          <Modal.Title>Ajouter une annonce</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input type="text" name="title" placeholder="Titre de l'annonce"
            value={this.state.title}
            onChange={event => this.setState( {title:event.target.value} )} />
          <textarea rows="4" cols="50"
            placeholder="Description de l'annonce"
            value={this.state.description}
            onChange={event => this.setState( {description:event.target.value} )}>
          </textarea>
          <p style={{ color: "orange"}}>*Veuilez entrer dans la description votre contact pour pouvoir être contacté</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Fermer
          </Button>
          <Button variant="primary" onClick={this.addingAnnonce}>
            Poster l'annonce
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalAddAnnonce;
