import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import axios from 'axios';
import { message } from 'antd';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('./../../config.json');

class ModalChangePassword extends Component {
    constructor(props) {
    super(props);
    this.state = {
      old_password: "",
      new_password: "",
      confirm_new_password: ""
    };

    this.handleChangePwd = this.handleChangePwd;
  }

  handleChangePwd = () => {
    if (this.state.old_password === "")
      message.error("Veuillez entrer votre ancien mot de passe")
    else if (this.state.new_password === "")
      message.error("Veuillez entrer votre nouveau mot de passe")
    else if (this.state.old_password === this.state.new_password)
      message.error("Vos mot de passe sont les mêmes")
    else if (this.state.confirm_new_password === "")
      message.error("Veuillez vérifier votre nouveau mot de passe")
    else if (this.state.new_password !== this.state.confirm_new_password)
      message.error("Le nouveau mot de passe et sa vérification sont différents");
    else {
      var params = new URLSearchParams();
      params.append("username", this.state.username);
      params.append("old_password", this.state.old_password);
      params.append("new_password", this.state.new_password);
      axios({
        method: "post",
        url: api_url.api_url + "/api/account/change_password",
        data: {
          "username": this.props.username,
          "old_password": this.state.old_password,
          "new_password": this.state.new_password }
      })
      .then(res => {
        if (res.data) {
          cookies.set('LoginToken', res.data);
          cookies.set('refresh', 'true');
        }
        if (this.state.stayConnected === true) {
          cookies.set('LoginRefreshToken', res.data.refresh_token);
          cookies.set('StayConnected', true);
        }
        else
          cookies.remove("LoginRefreshToken");
        cookies.set('StayConnected', false);
        this.props.onExitModal();
        message.success("Votre mot de passe a été changé");
        this.props.history.push("/home");
      })
      .catch(function (err) {
        if (err.response.data.old_password === "Invalid password") {
          message.error("Votre ancien mot de passe est éronné");
        }
        else if (err.response.data.new_password === "Invalid password") {
          message.error("Votre nouveau mot de passe est éronné");
        }
      });
    }
  }

  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{textAlign: "center"}} centered>
        <Modal.Header closeButton>
          <Modal.Title>Changer de mot de passe</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input type="password" id="old_password" name="old_password" placeholder="Ancien mot de passe"
            value={this.state.old_password} onChange={event => this.setState({ old_password: event.target.value })} />
          <input type="password" id="new_password" name="new_password" placeholder="Nouveau mot de passe"
            value={this.state.new_password} onChange={event => this.setState({ new_password: event.target.value })} />
          <input type="password" id="confirm_new_password" name="confirm_new_password" placeholder="Confirmer le nouveau mot de passe"
            value={this.state.confirm_new_password} onChange={event => this.setState({ confirm_new_password: event.target.value })} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Fermer
          </Button>
          <Button variant="primary" onClick={this.handleChangePwd}>
            Modifier le mot de passe
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalChangePassword;
