import React, { Component } from 'react';
import './../../Components/Profile Groups/profileGroups.css';
import axios from 'axios';
import { message } from 'antd';
import { Modal, Button } from 'react-bootstrap';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('../../config.json');

class ModalSheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sheet_name: "",
      sheet_description: ""
    };

    this.newSheet = this.newSheet.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  newSheet() {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/sheet/create",
      data: {
        "sheet_name": this.state.sheet_name,
        "sheet_description": this.state.sheet_description,
        "jwt": token.jwt
      }
    })
    .then(res => {
      this.props.onExitModal();
    })
    .catch(function (error) {
      message.error(error.response);
    });
  }

  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{textAlign: "center"}} centered>
        <Modal.Header closeButton>
          <Modal.Title>Créer une feuille de personnage</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input type="text" placeholder="Nom de la feuille" value={this.state.sheet_name} onChange={event => this.setState({sheet_name:event.target.value})}/>
          <input type="text" placeholder="Description" value={this.state.sheet_description} onChange={event => this.setState({sheet_description:event.target.value})}/>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Annuler
          </Button>
          <Button variant="primary" onClick={this.newSheet}>
            Créer la feuille
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalSheet;
