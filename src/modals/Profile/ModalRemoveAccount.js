import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { message } from 'antd';
import axios from 'axios';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('./../../config.json');

class ModalRemoveAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: ""
    };

    this.handleRemoveUser = this.handleRemoveUser.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleRemoveUser() {
    if (this.state.password === "")
      message.error("Veuillez rentrer votre mot de passe");
    else {
      axios({
        method: "post",
        url: api_url.api_url + "/api/account/remove",
        data: {
          "username": this.props.username,
          "password": this.state.password
        }
      })
      .then(res => {
        this.props.changePage();
        cookies.remove("LoginToken");
        cookies.set('refresh', 'true');
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
    }
  }

  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{textAlign: "center"}} centered>
        <Modal.Header closeButton>
          <Modal.Title>Supprimer son compte</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3 style={{color: "red"}}>Attention</h3>
            <h5 style={{color: "red"}}>Vous vous apprêtez à effacer votre compte.</h5>
            <br />
            <h6>Pour continuer veuillez entrez votre mot de passe ci-dessous :</h6>
            <input type="password" id="password" name="login" placeholder="Mot de Passe" value={this.state.password} onChange={event => this.setState({ password: event.target.value })} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Annuler
          </Button>
          <Button variant="outline-danger" onClick={this.handleRemoveUser}>
            Effacer son compte
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalRemoveAccount;
