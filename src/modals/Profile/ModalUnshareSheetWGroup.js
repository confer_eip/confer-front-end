import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { message } from 'antd';
import axios from 'axios';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('./../../config.json');

class ModalUnshareSheetWGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sheet_id: ""
    };

    this.handleUnshareSheet = this.handleUnshareSheet.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleUnshareSheet() {
    if (this.state.sheet_id === "")
      message.error("Veuillez entrer l'id d'une feuille de personnage");
    else {
      var token = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/group/unshare_sheet",
        data: {
          "jwt": token.jwt,
          "sheet_id": parseInt(this.state.sheet_id, 10),
          "group_id": parseInt(this.props.group_id, 10)
        }
      })
      .then(res => {
        message.success("La feuille n'est plus partagée");
        this.props.onExitModal();
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
    }
  }

  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{textAlign: "center"}} centered>
        <Modal.Header closeButton>
          <Modal.Title>Ne plus partager une feuille de personnage</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>Pour ne plus partager une feuille avec un groupe,<br /> veuillez rentrer l'id de la feuille ci-dessous :</h6>
          <br />
          <input type="text" id="d" name="sheet_id" placeholder="Id de la feuille" value={this.state.sheet_id} onChange={event => this.setState({ sheet_id: event.target.value })} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Annuler
          </Button>
          <Button variant="primary" onClick={this.handleUnshareSheet}>
            Ne plus partager la feuille
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalUnshareSheetWGroup;
