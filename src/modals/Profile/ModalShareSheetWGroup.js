import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import {message} from 'antd';
import axios from 'axios';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('./../../config.json');

class ModalShareSheetWGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sheet_id: ""
    };

    this.handleShareSheet = this.handleShareSheet.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleShareSheet() {
    if (this.state.sheet_id === "")
      message.error("Veuillez entrer l'id de la feuille de personnage");
    else {
      var token = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/group/share_sheet",
        data: {
          "jwt": token.jwt,
          "sheet_id": parseInt(this.state.sheet_id, 10),
          "group_id": parseInt(this.props.group_id, 10)
        }
      })
      .then(res => {
        message.success("La feuille a été partagée");
        this.props.onExitModal();
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
    }
  }

  render() {
    return (
      <Modal show={this.props.onShowModal} onHide={this.props.onExitModal} style={{textAlign: "center"}} centered>
        <Modal.Header closeButton>
          <Modal.Title>Partager une feuille de personnage</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>Pour partager une feuille avec un groupe,<br />veuillez rentrer l'id de la feuille ci-dessous :</h6>
          <br />
          <input type="text" id="d" name="sheet_id" placeholder="Id de la feuille" value={this.state.sheet_id} onChange={event => this.setState({ sheet_id: event.target.value })} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onExitModal}>
            Annuler
          </Button>
          <Button variant="primary" onClick={this.handleShareSheet}>
            Partager une feuille de personnage
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalShareSheetWGroup;
