import React, { Component } from "react";
import ModalViewAnnonce from './../../modals/Annonces/ModalViewAnnonce';
import ModalAddAnnonce from './../../modals/Annonces/ModalAddAnnonce';
import ModalModifyAnnonce from './../../modals/Annonces/ModalModifyAnnonce';
import { Button, CardDeck, Card } from 'react-bootstrap';
import axios from 'axios';
import Cookies from 'universal-cookie';
import '../../App.css';
export const cookies = new Cookies();
export
  var api_url = require('../../config.json');

const MODAL = {
  NONE: 'none',
  VIEW: 'view',
  ADD: 'add',
  MODIFY: 'modify',
}

export const ANNONCE_TYPE = {
  ALL: 'all',
  MINE: 'mine',
};

class Annonces extends Component {
  constructor(props) {
    super(props);
    this.state = {
      announces: [],
      page: 0,
      my_announce: false,
      announce: null,
      annonce_type: ANNONCE_TYPE.ALL
    };

    if (cookies.get('LoginToken')) {
      this.getAnnounce(0, false);
    }
    else {
      this.props.history.push("/");
    }
  }

  getAnnounce(page, mine) {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/announcement/get",
      data: {
        "number": 8,
        "page": page,
        "my_announcements": mine,
        "jwt": token.jwt
      }
    })
      .then(({ data }) => {
        if (data.announcements.length > 0){
          this.setState({
            announces: data.announcements,
            page: page
          })
        }
        else if (page === 0) {
          this.setState({
            announces: data.announcements,
            page: page
          })
        }
        return (data.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  }

  previousPage = () => {
    if (this.state.page !== 0) {
      this.getAnnounce(this.state.page - 1, this.state.my_announce);
    };
  }

  nextPage = () => {
    this.getAnnounce(this.state.page + 1, this.state.my_announce);
  }

  showMyAnnonce = () => {
    this.getAnnounce(0, true);
    this.setState({
      my_announce: true,
      page: 0,
      annonce_type: ANNONCE_TYPE.MINE
    });
  }

  showAllAnnonce = () => {
    this.getAnnounce(0, false);
    this.setState({
      my_announce: false,
      page: 0,
      annonce_type: ANNONCE_TYPE.ALL
    });
  }

  showViewAnnonce = (announce) => {
    this.setState({ showModal: MODAL.VIEW, announce: announce });
  }

  exitViewAnnonce = () => {
    this.setState({ showModal: MODAL.NONE });
  }

  showAddAnnonce = () => {
    this.setState({ showModal: MODAL.ADD });
  }

  exitAddAnnonce = () => {
    this.setState({ showModal: MODAL.NONE });
  }

  showModifyAnnonce = (announce) => {
    this.setState({ showModal: MODAL.MODIFY, announce: announce });
  }

  exitModifyAnnonce = () => {
    this.setState({ showModal: MODAL.NONE });
  }

  render() {
    return (
      <div className="Annonces">
      {this.state.annonce_type === ANNONCE_TYPE.ALL ? (
        <h1>Annonces</h1> ) : ( <h1>Mes annonces</h1>
        )}
        <Button style={{ backgroundImage: "#0098f0", marginRight: "2px"}} onClick={this.showAddAnnonce}>Ajouter une annonce</Button>
        {this.state.annonce_type === ANNONCE_TYPE.ALL ? (
          <Button className="Button-space-small-screen" style={{ backgroundImage: "#0098f0"}} onClick={this.showMyAnnonce}>Voir mes annonces</Button>
          ):(
          <Button className="Button-space-small-screen" style={{ backgroundImage: "#0098f0"}} onClick={this.showAllAnnonce}>Voir toutes les annonces</Button>
        )}

        <div className="Annonces-holder justify-content-center">
          {this.state.annonce_type === ANNONCE_TYPE.ALL ? (
            this.state.announces.map(announce => (
              <div className="Block-annonce" key={announce.id} style={{ marginBottom: '60px', }} onClick={this.showViewAnnonce.bind(this, announce)}>
                <CardDeck >
                  <Card style={{color: "white"}}>
                    <Card.Header style={{ background: "#252525", fontWeight: "300"}}><b>{announce.advertiser_name}</b></Card.Header>
                    <Card.Body style={{ backgroundColor: "#318CE7"}}>
                      <Card.Title>{announce.title}</Card.Title>
                        <Card.Text >{announce.description.substring(0, 35)}...</Card.Text>
                      <Button style={{ background: "#003366", borderColor: "#2c75ff"}}>Voir l'annonce</Button>
                    </Card.Body>
                  </Card>
                </CardDeck>
              </div>
            ))
          ):(
          this.state.announces.map(announce => (
            <div className="Block-annonce" key={announce.id} style={{ marginBottom: '60px', }} onClick={this.showModifyAnnonce.bind(this, announce)}>
              <CardDeck >
                <Card style={{color: "white"}}>
                  <Card.Header style={{ backgroundColor: "#252525", fontWeight: "300"}}><b>{announce.advertiser_name}</b></Card.Header>
                  <Card.Body style={{ backgroundColor: "#318CE7"}}>
                    <Card.Title>{announce.title}</Card.Title>
                      <Card.Text >{announce.description.substring(0, 35)}...</Card.Text>
                      <Button style={{ background: "#003366", borderColor: "#2c75ff"}}>Modifier l'annonce</Button>
                  </Card.Body>
                </Card>
              </CardDeck>
            </div>
          )))}
        </div>

        { this.state.announces.length > 0 ? (
          <nav aria-label="Page" style={{ marginBottom: "80px" }}>
            <ul className="pagination justify-content-center">
              <li className="page-item" style={{ width: "100px" }}>
                <Button variant="primary" onClick={this.previousPage}>Precedent</Button>
              </li>
              <li className="page-item" style={{ width: "100px" }}>
                <Button variant="primary" onClick={this.nextPage}>Suivant</Button>
              </li>
            </ul>
          </nav>
        ) : <h6 style={{ marginTop: "40px" }}>Aucune annonce n'a encore été posté</h6> }

        {this.state.showModal === MODAL.VIEW ? <ModalViewAnnonce onExitModal={this.exitViewAnnonce} onShowModal={true} announcement={this.state.announce} /> : null}
        {this.state.showModal === MODAL.ADD ? <ModalAddAnnonce onExitModal={this.exitAddAnnonce} onShowModal={true} /> : null}
        {this.state.showModal === MODAL.MODIFY ? <ModalModifyAnnonce onExitModal={this.exitModifyAnnonce} onShowModal={true} announcement={this.state.announce} /> : null}
      </div>
    );
  }
}

export default Annonces;
