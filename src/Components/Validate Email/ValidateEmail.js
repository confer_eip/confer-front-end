import React, { Component } from "react";
import { Image } from 'react-bootstrap';
import logo from '../../img/Logo.jpg';
import axios from 'axios';
import Cookies from 'universal-cookie';
import { message } from 'antd';
export const cookies = new Cookies();
var api_url = require('../../config.json');

class ValidateEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      code: ""
    };
    
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange;
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleClick() {
    if (this.state.email === "")
      message.error("Veuillez rentrer une adresse email")
    else if (this.state.password === "")
      message.error("Veuillez rentrer un mot de passe")
    else if (this.state.code === "")
      message.error("Veuillez rentrer le code reçu par email")
    else {
      var params = new URLSearchParams();
      params.append("username", this.state.username);
      params.append("password", this.state.password);
      params.append("code", this.state.code);
      axios({
        method: "post",
        url: api_url.api_url + "/api/account/validate_email",
        data: {
          "username": this.state.username,
          "password": this.state.password,
          "code": this.state.code
        } 
      })
      .then(res => {
        if (res.data) {
          cookies.set('LoginToken', res.data);
          cookies.set('refresh', 'true');
        }
        if (this.state.stayConnected === true) {
          cookies.set('LoginRefreshToken', res.data.refresh_token);
          cookies.set('StayConnected', true);
        }
        else
          cookies.remove("LoginRefreshToken");
        cookies.set('StayConnected', false);
        this.props.history.push("/home");
      })
      ;
    }
  }
  render() {
    return (
        <div className="wrapper fadeInDown">
        <div id="formContent">
          <div className="fadeIn first">
          <Image src={logo} id="icon" alt="User Icon" />
          </div>
          <div>
            <input type="text" id="login" className="fadeIn second" name="login" placeholder="pseudo" value={this.state.username} onChange={event => this.setState({username:event.target.value})}/>
            <input type="password" id="password" className="fadeIn third" name="login" placeholder="mot de passe" value={this.state.password} onChange={event => this.setState({password:event.target.value})}/>
            <input type="text" id="code" className="fadeIn second" name="code" placeholder="code reçu par email" value={this.state.code} onChange={event => this.setState({code:event.target.value})}/>
            <input type="submit" className="fadeIn fourth" value="Confirmer son compte" onClick={this.handleClick}/>
          </div>
        </div>
      </div>
    );
  }
}

export default ValidateEmail;
