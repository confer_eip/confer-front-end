import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Image } from 'react-bootstrap';
import logo from '../../img/Logo.jpg';
import axios from 'axios';
import Cookies from 'universal-cookie';
import { message } from 'antd';
export const cookies = new Cookies();
var api_url = require("../../config.json");

class Connexion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
    if (cookies.get('refresh') === 'true') {
      cookies.set('refresh', 'false');
      window.location.reload();
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {

    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleClick() {
    if (this.state.username === "")
      message.error("Veuillez rentrer un username ou une adresse mail")
    else if (this.state.password === "")
      message.error("Veuillez rentrer un mot de passe")
    else {
      var params = new URLSearchParams();
      params.append("username", this.state.username);
      params.append("password", this.state.password);
      params.append("email", this.state.email);
      axios({
        method: "post",
        url: api_url.api_url + "/api/account/login",
        data: { "username": this.state.username, "password": this.state.password }
      })
        .then(res => {
          if (res.data) {
            cookies.set('LoginToken', res.data);
            cookies.set('refresh', 'true');
          }
          if (this.state.stayConnected === true) {
            cookies.set('LoginRefreshToken', res.data.refresh_token);
            cookies.set('StayConnected', true);
          }
          else
            cookies.remove("LoginRefreshToken");
          cookies.set('StayConnected', false);
          this.props.history.push("/home");
        })
        .catch(function (err) {
          if (err.response.data.username === "Unknown username or email") {
            message.error("Le pseudo/adresse mail ou le mot de passe est éronné");
          }
          else if (err.response.data.password === "Invalid password") {
            message.error("Le pseudo/adresse mail ou le mot de passe est éronné");
          }
        })
        ;
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
  }

  render() {
    return (
      <div className="wrapper fadeInDown">
        <div id="formContent">
          <div className="fadeIn first">
            <Image src={logo} id="icon" alt="User Icon" />
          </div>
          <form onSubmit={this.handleSubmit}>
            <input type="text" id="login" className="fadeIn second" name="login" placeholder="Identifiant" value={this.state.username} onChange={event => this.setState({ username: event.target.value })} />
            <input type="password" id="password" className="fadeIn third" name="password" placeholder="Mot de passe" value={this.state.password} onChange={event => this.setState({ password: event.target.value })} />
            <input type="submit" className="fadeIn fourth" value="Connexion" onClick={this.handleClick} />
          </form>
          <div id="formFooter">
            <NavLink to="/forgottenpwd"><a className="underlineHover" href="/">Mot de passe oublié ?</a></NavLink>
          </div>
        </div>
        <div style={{ marginBottom: "50px" }} />
      </div>
    );
  }
}

export default Connexion;
