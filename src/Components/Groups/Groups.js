import React, { Component } from "react";
import { CardDeck, Card, Button, Row, Col, Table } from 'react-bootstrap';
//import { message } from 'antd';
import axios from 'axios';
import {message} from 'antd';
import ModalCreateGroups from './../../modals/MyGroups/ModalCreateGroups';
import ModalManageGroupSheets from './../../modals/MyGroups/ModalManageGroupSheets';
import ModalManageGroups from './../../modals/MyGroups/ModalManageGroups';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('./../../config.json');

const MODAL = {
  NONE: 'none',
  CREATE: 'create',
  MANAGE: 'manage',
  MANAGE_SHEETS: 'manage_sheets'
}

class Groups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      user_id: '',
      groups: [],
      invitations: [],
      groups_details: [],
      showModal: MODAL.NONE,
      modal_group_id: null,
      modal_group_name: null
    };

    this.RequestProfile();
  }

  RequestProfile() {
    if (cookies.get('refresh') === 'true') {
      cookies.set('refresh', 'false');
      window.location.reload();
    }
    if (cookies.get('LoginToken')) {
      const cookie = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/account/profile",
        data: cookie
      })
        .then(({ data }) => {
          this.setState({
            username: data.username,
            email: data.email,
            groups: data.groups,
            user_id: data.user_id,
            invitations: data.invitations,
            sheets: data.sheets
          })

          this.getGroupsInformations();
        })
        .catch((error) => {
          console.error(error)
        });
    }
    else {
      this.props.history.push("/");
    }
  }

  getGroupsInformations = () => {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/group/informations",
      data: {
        "jwt": token.jwt,
        "groups_id": this.state.groups
      }
    })
      .then(({ data }) => {
        this.setState({ groups_details: data.groups });
      })
      .catch((error) => {
        console.error(error)
      });
  }

  showCreateGroups = () => {
    this.setState({ showModal: MODAL.CREATE });
  }

  exitCreateGroups = () => {
    this.setState({ showModal: MODAL.NONE });
    this.RequestProfile();
  }

  showManageGroups(group_id, group_name, owner_id) {
    this.setState({
      showModal: MODAL.MANAGE,
      modal_group_infos: {
        group_id: group_id,
        group_name: group_name,
        group_owner_id: owner_id
      }
    });
  }

  exitManageGroups = () => {
    this.setState({ showModal: MODAL.NONE });
    this.RequestProfile();
  }

  showManageGroupSheets = (group_id) => {
    this.setState({ showModal: MODAL.MANAGE_SHEETS, modal_group_id: group_id });
  }

  exitManageGroupSheets = () => {
    this.setState({ showModal: MODAL.NONE });
    this.RequestProfile();
  }

  joinGroup(invitation) {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/group/join",
      data: {
        "group_id": invitation,
        "jwt": token.jwt
      }
    })
      .then(res => {
        message.success("Vous avez rejoint le groupe avec succès");
        this.RequestProfile();
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
  }

  refuseJoinGroup(invitation) {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/group/decline",
      data: {
        "group_id": invitation,
        "jwt": token.jwt
      }
    })
      .then(res => {
        this.RequestProfile();
        message.success("Vous avez refusé l'invitation avec succès");
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
  }

  showGroupSheets = (group_id) => {
    this.props.history.push("/groupsheets/" + group_id);
  }

  render() {
    return (
      <div style={{ margin: "0 10%" }}>
        <h1 style={{ textAlign: 'center', marginBottom: '40px' }}>Mes groupes</h1>
        <Button variant="primary" style={{ marginBottom: '20px' }} onClick={this.showCreateGroups}>Créer un groupe</Button>
        {this.state.group_details}
        <div>
          {this.state.invitations.map(invitation => (
            <Card key={invitation}
              text="white" style={{ backgroundColor: "#1f2251", marginBottom: "10px" }}>
              <Card.Body>
                <p>Invitation au groupe : <b>{invitation}</b></p>
                <Button className="Button-space-small-screen" style={{ marginRight: "10px" }}
                  onClick={this.joinGroup.bind(this, invitation)}>
                  Rejoindre le groupe
              </Button>
                <Button variant="danger" onClick={this.refuseJoinGroup.bind(this, invitation)}>
                  Décliner l'invitation
              </Button>
              </Card.Body>
            </Card>
          ))}
        </div>

        <CardDeck style={{ marginBottom: "100px" }}>
          {this.state.groups_details.map(elem => (
            <Card key={elem.group_id} text="white"
              style={{ minWidth: "18rem", marginBottom: "40px" }}>
              <Card.Header style={{ backgroundColor: "#252525" }}>
                <Row>
                  <Col md={{ span: 2, offset: 0 }}>
                    ID: {elem.group_id}
                  </Col>
                  <Col md={{ span: 8, offset: 0 }}>
                    {elem.group_name.toUpperCase()}
                  </Col>
                </Row>
              </Card.Header>
              <Card.Body style={{ backgroundColor: "#318CE7" }}>
                <Row>
                  <Col md={{ span: 6 }} style={{ fontWeight: "bold", display: "flex", justifyContent: "flex-end" }}>
                    Propriétaire :
                    </Col>
                  <Col md={{ span: 6 }} style={{ display: "flex", justifyContent: "flex-start" }}>
                    {elem.members.find(function (mem) {
                      return mem.user_id === elem.owner_id;
                    }).username}
                  </Col>
                </Row>
                <Row>
                  <Col md={{ span: 12 }} style={{ fontWeight: "bold" }}>
                    Membres
                  </Col>
                </Row>
                <Row>
                  <Col md={{ span: 8, offset: 2 }} >
                    <Table size="sm" bordered >
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Pseudo</th>
                        </tr>
                      </thead>
                      <tbody>
                        {elem.members.map(member => (
                          <tr key={member.user_id}>
                            <th>{member.user_id}</th>
                            <th>{member.username}</th>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </Col>
                </Row>
                {(elem.invitations.length !== 0) ?
                  <div>
                    <Row>
                      <Col md={{ span: 12 }} style={{ fontWeight: "bold" }}>
                        Joueurs invités
                      </Col>
                    </Row>
                    <Row>
                      <Col md={{ span: 8, offset: 2 }} >
                        <Table size="sm" bordered >
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Pseudo</th>
                            </tr>
                          </thead>
                          <tbody>
                            {elem.invitations.map(member => (
                              <tr key={member.user_id}>
                                <th>{member.user_id}</th>
                                <th>{member.username}</th>
                              </tr>
                            ))}
                          </tbody>
                        </Table>
                      </Col>
                    </Row>
                  </div> : null}


                <Button style={{ background: "#003366", borderColor: "#2c75ff" }}
                  onClick={this.showManageGroups.bind(this, elem.group_id, elem.group_name, elem.owner_id)}>
                  Modifier
                </Button>
                <Button style={{ background: "#003366", borderColor: "#2c75ff" }}
                  onClick={this.showManageGroupSheets.bind(this, elem.group_id)}>
                  Gérer les feuilles
                </Button>
                <Button style={{ background: "#003366", borderColor: "#2c75ff" }}
                  onClick={this.showGroupSheets.bind(this, elem.group_id)}>
                  Voir les feuilles du groupe
                </Button>
              </Card.Body>
            </Card>
          ))}
        </CardDeck>

        {
          this.state.showModal === MODAL.CREATE
            ? <ModalCreateGroups onExitModal={this.exitCreateGroups} onShowModal={true} /> : null
        }
        {
          this.state.showModal === MODAL.MANAGE
            ? <ModalManageGroups onExitModal={this.exitManageGroups} onShowModal={true}
              group_infos={this.state.modal_group_infos} user_id={this.state.user_id} /> : null
        }
        {
          this.state.showModal === MODAL.MANAGE_SHEETS
            ? <ModalManageGroupSheets onExitModal={this.exitManageGroupSheets} onShowModal={true} history={this.props.history} group_id={this.state.modal_group_id} /> : null
        }
      </div>
    );
  }
}

export default Groups;
