import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import axios from 'axios';
import Cookies from 'universal-cookie';
import "./LandingPage.css";
export const cookies = new Cookies();
var api_url = require('../../config.json');

class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: ""
    };

    if (cookies.get('LoginToken')) {
      const cookie = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/account/profile",
        data: cookie
      })
        .then(({ data }) => {
          this.setState({
            username: data.username,
            email: data.email
          })
        })
        .catch((error) => {
          console.error(error)
        });
    }
    else {
      this.props.history.push("/");
    }
  }

  go_sign() {
  }

  render() {
    return (
      <div>
        <div id="f1">
          <div className="container">
            <div className="row">
              <div className="col-xl-9 mx-auto">
                <h1 id="ivory">Confer</h1>
                <p id="ivory">Le site qui vous permet de gérer vos différentes feuilles de personnage quel que soit le JDR</p>
              </div>
              <div className="col-md-10 col-lg-8 col-xl-7 mx-auto">
                <form>
                  <div className="form-row">
                    <div className="col-10 col-md-6 button-style">
                      <button className="button-style-inscription"><NavLink to="/inscription"><input type="image" id="bt_sign" src={require("../../img/register.png")} alt="dice_image"/></NavLink></button>
                      <p className="text">Inscription</p>
                    </div>
                    <div className="col-10 col-md-6 button-style">
                    <button className="button-style-inscription"><NavLink to="/connexion"><input type="image" id="bt_sign" src={require("../../img/user.png")} alt="dice_image" /></NavLink></button>
                    <p className="text">Connexion</p>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div id="f2">
          <div className="container">
            <div className="row">
              <div className="col-xl-9 mx-auto">
                <h1 id="ivory">Galerie</h1>
                <p id="ivory">Vous cherchez une feuille de personnage pour un JDR spécifique ?</p>
                <p id="ivory">Regardez dans notre galerie !</p>
              </div>
              <div className="col-md-10 col-lg-8 col-xl-7 mx-auto button-style">
                <form>
                <button className="button-style-galerie"><NavLink to="/galerie"><input type="image" id="bt_sign" src={require("../../img/template.png")} alt="dice_image" /></NavLink></button>
                <p className="text">Galerie</p>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div >
    );
  }
}

export default LandingPage;
