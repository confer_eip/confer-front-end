import React from "react";
import { WidthProvider } from "react-grid-layout";
import { message, Popover, Menu, Dropdown, Icon, Button, Radio, Checkbox, Input, Row, Col } from 'antd';
import GridLayout from "react-grid-layout";
import _ from "lodash";
import reactCSS from 'reactcss';
import "./StaticGrid.css";
import "react-grid-layout/css/styles.css";
import "react-resizable/css/styles.css";
import "antd/dist/antd.css";
import { SketchPicker } from 'react-color';
import Modal from 'react-bootstrap/Modal';
import Badge from 'react-bootstrap/Badge';
import axios from 'axios';
import { evaluate } from "mathjs";


import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('../../config.json');

const ButtonGroup = Button.Group;
const GridStaticWitdth = WidthProvider(GridLayout);
const MODE = {
    STATIC: 'static',
    PLAY: 'play',
    EDIT: 'edit'
};

const TYPE = {
    INPUT: 'input',
    LABEL: 'label',
    EQUAT: 'equation'
};

let sheetData;
let tagSheet;

function cleanSource(source) {
    return _.map(source, function (item) {
        return {
            x: item.x,
            y: item.y,
            w: item.w,
            h: item.h,
            type: item.type,
            content: item.content,
        }
    })
}

function updateSource(source, layout) {
    return _.map(source, function (sourceItem) {
        var layoutItem = _.find(layout, function (layoutItem) {
            return layoutItem.i === sourceItem.i;
        })
        if (layoutItem !== undefined) {
            sourceItem.x = layoutItem.x;
            sourceItem.y = layoutItem.y;
            sourceItem.w = layoutItem.w;
            sourceItem.h = layoutItem.h;
        }
        return sourceItem;
    });
}

function getSource(props) {
    if (cookies.get('LoginToken')) {
        if (props.location.data) {
            sheetData = props.location.data;
            if (props.location.data.template_id)
                props.location.data.sheet_data = props.location.data.template_data;
            if (props.location.data.sheet_data === "")
                return [];
            let array = JSON.parse(props.location.data.sheet_data);
            return (_.map(array, (item, index) => {
                item.i = index.toString();
                item.isDraggable = true;
                item.isResizable = true;
                if (item.type === TYPE.EQUAT) {
                    Object.defineProperties(item.content, {
                        'value': {
                            get: function () {
                                return calculEquat(replaceTag(this.equat));
                            }
                        },
                    });
                }
                return item;
            }));
        }
        else
            props.history.goBack();
    } else
        props.history.push("/");
}

function replaceTag(el) {
    if (typeof el !== "string")
        return "ERROR";
    else {
        let array = _.clone(el);
        array = array.replace(/\s\s+/g, ' ');
        array = array.split(' ');
        let newValue = "";
        while (array.length !== 0) {
            let elem = array.shift()
            if (elem.charAt(0) === '@') {
                let tag = _.find(tagSheet, function (elemSource) {
                    if (elemSource.content.isTagActive && elemSource.content.tag === elem.substring(1))
                        return true
                    else
                        return false
                });
                if (tag === undefined)
                    newValue += " " + elem;
                else
                    newValue += " " + tag.content.value;
            }
            else
                newValue += " " + elem;
        }
        return newValue;
    }
}

function calculEquat(str) {
    try {
        return evaluate(str);
    } catch {
        return "INVALIDE!";
    }
}


class StaticGrid extends React.PureComponent {
    static defaultProps = {
        className: "layout",
        cols: 12,
        rowHeight: 38,
        margin: [2, 2],
        compactType: null,
        preventCollision: true,
        draggableCancel: ".noDrag"
    };

    constructor(props) {
        super(props);
        this.state = {
            source: getSource(this.props),
            currentMode: MODE.STATIC,
            newCounter: 0,
            editCell: { content: { value: "", backgroundColor: "", tag: "", isTagActive: false } },
            visibleModal: false,
            displayColorPicker: false,
            color: "#000000",
            selectCell: TYPE.LABEL,
            equatTmp: "",
            isFromGalerie: false,
        };
        this.updateEquat = _.debounce(this.updateEquat, 1000);

        if (this.state.source)
            tagSheet = this.state.source;
        window.scrollTo(0, 0)

        if (props.location && props.location.data) {
            if (props.location.data.isFromGalerie === true)
                this.state.isFromGalerie = true;
            if (props.location.data.mode) {
                switch (props.location.data.mode) {
                    case (MODE.PLAY):
                        this.state.currentMode = MODE.PLAY;
                        break;
                    case (MODE.EDIT):
                        this.state.currentMode = MODE.EDIT;
                        break;
                    case (MODE.STATIC):
                    default:
                        this.state.currentMode = MODE.STATIC;
                        break;
                }
            }
        }
        this.onAddItem = this.onAddItem.bind(this);
        this.onLayoutChange = this.onLayoutChange.bind(this);

        this.handleModeChange = this.handleModeChange.bind(this);
        this.handleClickColorPicker = this.handleClickColorPicker.bind(this);
        this.handleCloseColorPicker = this.handleCloseColorPicker.bind(this);
        this.handleChangeColorPicker = this.handleChangeColorPicker.bind(this);
        this.handleMenuClick = this.handleMenuClick.bind(this);

        this.saveSheet = this.saveSheet.bind(this);
    }

    componentDidUpdate() {
        tagSheet = this.state.source;
    }

    saveSheet() {
        let token = cookies.get('LoginToken');
        if (!token) {
            alert("Veuillez vous connecter");
        }
        let sheet = sheetData;
        message.loading("Sauvegarde en cours...", 50)
        if (this.state.isFromGalerie) {
            axios({
                method: "post",
                url: api_url.api_url + "/api/sheet/create-from-template",
                data: {
                    "template_id": sheet.template_id,
                    "jwt": token.jwt
                }
            })
                .then(({ data }) => {
                    /*        alert("Cette feuille de personnage a été ajoutée");
                           window.location.reload(); */
                    message.destroy();
                    message.success('Ajout réussie !', 1);
                })
                .catch(function (error) {
                    message.destroy();
                    message.error('Echec de la Sauvegarde', 2);
                });
        }
        else
            axios({
                method: "post",
                url: api_url.api_url + "/api/sheet/save",
                data: {
                    "jwt": token.jwt,
                    "sheet_id": sheet.sheet_id,
                    "sheet_data": JSON.stringify(cleanSource(_.map(updateSource(this.state.source, this.state.layout))))
                }
            })
                .then(res => {
                    message.destroy();
                    message.success('Sauvegarde réussie !', 1);
                })
                .catch(function (error) {
                    message.destroy();
                    message.error('Echec de la Sauvegarde', 2);
                });
    }

    createElement(el) {
        return (
            <div key={el.i} data-grid={el}
                style={{ background: el.content.backgroundColor }}>
                <Row style={{ height: "100%" }} type="flex" justify="center" align="middle" >
                    <Col span={24}>
                        {(() => {
                            switch (this.state.currentMode) {
                                case MODE.STATIC:
                                case MODE.PLAY:
                                    switch (el.type) {
                                        case TYPE.INPUT:
                                            return <Input className="noDrag cellInput"
                                                value={el.content.value}
                                                disabled={this.state.currentMode === MODE.STATIC}
                                                onChange={this.updateInput.bind(this, el.i)}
                                            />;
                                        case TYPE.EQUAT:
                                        case TYPE.LABEL:
                                            return <div style={{ paddingTop: "2px" }}>
                                                {el.content.value}
                                            </div>;
                                        default:
                                            return <div>Error</div>;
                                    }
                                case MODE.EDIT:
                                    return <div onDoubleClick={this.focusInput.bind(this, el)}>
                                        <Input className="noDrag cellInput"
                                            value={el.content.value}
                                            onChange={(el.type !== TYPE.EQUAT) ? this.updateInput.bind(this, el.i) : null}
                                        /></div>
                                default:
                                    return;
                            }
                        })()}
                    </Col>
                </Row>
            </div>
        );
    }

    onAddItem(type) {
        let bottom = 0;
        if (this.state.layout && this.state.layout.length > 0)
            this.state.layout.forEach(elem => {
                if (elem.y + elem.h > bottom)
                    bottom = elem.y + elem.h;
            });
        let newItem = {
            i: "n" + this.state.newCounter,
            x: 0,
            y: bottom,
            w: 1,
            h: 1,
        };
        switch (type) {
            case TYPE.INPUT:
                newItem.type = TYPE.INPUT;
                newItem.content = { value: "", backgroundColor: "#D3D3D3", tag: "", isTagActive: false };
                break;
            case TYPE.LABEL:
                newItem.type = TYPE.LABEL;
                newItem.content = { value: "", backgroundColor: "#D3D3D3", tag: "", isTagActive: false };
                break;
            case TYPE.EQUAT:
                newItem.type = TYPE.EQUAT;
                newItem.content = {
                    get value() {
                        return calculEquat(replaceTag(this.equat));
                    },
                    equat: "", backgroundColor: "#D3D3D3", tag: "", isTagActive: false
                };
                break;
            default:
                newItem = null;
                break
        }
        if (newItem !== null)
            this.setState({
                source: this.state.source.concat(newItem),
                newCounter: this.state.newCounter + 1
            });
    }

    onLayoutChange(layout) {
        this.setState({ layout: layout });
    }

    onRemoveItem(i) {
        this.setState({ source: _.reject(this.state.source, { i: i }) });
    }

    updateInput(i, event) {
        this.setState({
            source: _.map(this.state.source, function (item) {
                if (item.i === i)
                    item.content.value = event.target.value;
                return item
            })
        });
    }


    handleModeChange = e => {
        let tmp = _.map(
            updateSource(this.state.source, this.state.layout)
            , function (item, index) {
                item.i = index.toString() + ((e.target.value === MODE.EDIT) ? "S" : "E")
                item.isDraggable = (e.target.value === MODE.EDIT) ? true : false;
                item.isResizable = (e.target.value === MODE.EDIT) ? true : false;
                return (item);
            })
        this.setState({
            source: tmp,
            currentMode: e.target.value,
        });
    }

    focusInput(el, event) {
        this.setState({
            editCell: _.cloneDeep(el),
            visibleModal: true,
            color: _.cloneDeep(el.content.backgroundColor),
            equatTmp: (el.type === TYPE.EQUAT) ? replaceTag(el.content.equat) : "",
        });
    }

    ModalReset = () => {
        this.setState({ visibleModal: false });
    };

    ModalSave(i) {
        let editCell = this.state.editCell;
        if (editCell.content.isTagActive) {
            if (_.every(this.state.source, function (item) {
                if (item.i !== editCell.i && item.content.isTagActive) {
                    return (item.content.tag === editCell.content.tag) ? false : true;
                }
                else
                    return true
            })) {
            }
            else {
                message.error("Tag déjà existant", 2);
                return;
            }

        }

        this.setState({
            source: _.map(this.state.source, function (item) {
                if (item.i === i) {
                    item.content = editCell.content;
                    if (item.type === TYPE.EQUAT) {
                        Object.defineProperties(item.content, {
                            'value': {
                                get: function () {
                                    return calculEquat(replaceTag(this.equat));
                                }
                            },
                        });
                    }
                }
                return item
            })
        });
        this.setState({ visibleModal: false });
    };

    ModalonRemoveItem(i) {
        this.setState({ source: _.reject(this.state.source, { i: i }) });
        this.ModalReset();
    }

    ModalChangeValue(event) {
        event.persist();
        this.setState(prevState => ({
            editCell: {
                ...prevState.editCell,
                content: {
                    ...prevState.editCell.content,
                    value: event.target.value
                }
            }
        }))
    }

    ModalChangeEquation(event) {
        event.persist();
        this.setState(prevState => ({
            editCell: {
                ...prevState.editCell,
                content: {
                    ...prevState.editCell.content,
                    equat: event.target.value
                }
            }
        }))
        this.updateEquat();
    }

    ModalChangeColor(color) {
        this.setState(prevState => ({
            editCell: {
                ...prevState.editCell,
                content: {
                    ...prevState.editCell.content,
                    backgroundColor: color.hex
                }
            }
        }))
    }

    ModalChangeIsTagActive(event) {
        this.setState(prevState => ({
            editCell: {
                ...prevState.editCell,
                content: {
                    ...prevState.editCell.content,
                    isTagActive: event.target.checked
                }
            }
        }))
    }

    ModalChangeTag(event) {
        event.persist();
        this.setState(prevState => ({
            editCell: {
                ...prevState.editCell,
                content: {
                    ...prevState.editCell.content,
                    tag: event.target.value
                }
            }
        }))
    }

    handleClickColorPicker() {
        this.setState({
            displayColorPicker: !this.state.displayColorPicker,
            color: this.props.color
        });
    };

    handleCloseColorPicker() {
        this.setState({ displayColorPicker: false });
    };

    handleChangeColorPicker(color) {
        this.setState({ color: color.hex });
    };

    handleMenuClick(e) {
        this.setState({ selectCell: e.key });
    }

    updateEquat() {
        this.setState({
            equatTmp: replaceTag(this.state.editCell.content.equat)
        })

    }

    render() {
        const { visibleModal } = this.state;
        const styles = reactCSS({
            'default': {
                color: {
                    width: '36px',
                    height: '14px',
                    borderRadius: '2px',
                    background: `${this.state.color}`,
                },
                swatch: {
                    padding: '1px',
                    background: '#fff',
                    borderRadius: '1px',
                    boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
                    display: 'inline-block',
                    cursor: 'pointer',
                },
                popover: {
                    position: 'absolute',
                    zIndex: '2',
                },
                cover: {
                    position: 'fixed',
                    top: '0px',
                    right: '0px',
                    bottom: '0px',
                    left: '0px',
                },
            }
        });

        const colorPicker = <SketchPicker
            style={{ width: "500px" }}
            color={this.state.editCell.content.backgroundColor}
            onChange={this.handleChangeColorPicker}
            onChangeComplete={this.ModalChangeColor.bind(this)} />

        const menu =
            <Menu onClick={this.handleMenuClick}>
                <Menu.Item key={TYPE.INPUT}>
                    Entrée
              </Menu.Item>
                <Menu.Item key={TYPE.LABEL}>
                    Label
              </Menu.Item>
                <Menu.Item key={TYPE.EQUAT}>
                    Equation
              </Menu.Item>
            </Menu>;

        return (
            <div style={{ fontSize: "16px" }}>
                <div style={{ height: "5px" }} />
                {(this.state.isFromGalerie) ?
                    <Row type="flex" justify="center">
                        <Col span={24} style={{ display: "flex", justifyContent: "center" }}>
                            <Button onClick={this.saveSheet} >Sauvegarder</Button>
                        </Col>
                    </Row>
                    :
                    <Row type="flex" justify="center">
                        <Col span={8} style={{ display: "flex", justifyContent: "flex-end" }}>
                            <Radio.Group value={this.state.currentMode} onChange={this.handleModeChange}>
                                <Radio.Button value={MODE.STATIC}  >Statique</Radio.Button>
                                <Radio.Button value={MODE.PLAY}  >Jouer</Radio.Button>
                                <Radio.Button value={MODE.EDIT} >Modifier</Radio.Button>
                            </Radio.Group>
                        </Col>
                        <Col span={8} style={{ display: "flex", justifyContent: "flex-end" }}>
                            <ButtonGroup >
                                <Button onClick={() => this.onAddItem(this.state.selectCell)} disabled={(this.state.currentMode !== MODE.EDIT)}>
                                    <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                                        <Icon type="plus" />&nbsp;&nbsp;
                                    {(() => {
                                            switch (this.state.selectCell) {
                                                case TYPE.INPUT:
                                                    return <div>Entrée</div>;
                                                case TYPE.LABEL:
                                                    return <div>Label</div>;
                                                case TYPE.EQUAT:
                                                    return <div>Equation</div>;
                                                default:
                                                    return <div>ERREUR</div>;
                                            }
                                        })()}
                                    </div>
                                </Button>
                                <Dropdown overlay={menu} placement="bottomRight" disabled={(this.state.currentMode !== MODE.EDIT)}>
                                    <Button >
                                        <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                                            <Icon type="down" />
                                        </div>
                                    </Button>
                                </Dropdown>
                            </ButtonGroup>
                        </Col>
                        <Col span={8} style={{ display: "flex", justifyContent: "flex-start" }}>
                            <Button onClick={this.saveSheet} disabled={(this.state.currentMode === MODE.STATIC)} style={{ marginLeft: "5px" }}>Sauvegarder</Button>
                        </Col>
                    </Row>
                }
                <div style={{ height: "5px" }} />
                <Row>
                    <Col span={24}>
                        <GridStaticWitdth
                            onLayoutChange={this.onLayoutChange}
                            {...this.props}>
                            {_.map(this.state.source, el => this.createElement(el))}
                        </GridStaticWitdth>
                    </Col>
                </Row>
                <div style={{ height: "70px" }} />

                <Modal show={visibleModal} onHide={this.ModalReset} centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Modification cellule <Badge variant="secondary">
                            {(() => {
                                switch (this.state.editCell.type) {
                                    case TYPE.INPUT:
                                        return <div>Entrée</div>;
                                    case TYPE.LABEL:
                                        return <div>Label</div>;
                                    case TYPE.EQUAT:
                                        return <div>Equation</div>;
                                    default:
                                        return <div />;
                                }
                            })()}
                        </Badge> </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <Col span={24} style={{ display: "flex", alignItems: "center", fontSize: "16px" }}>
                                Tag&nbsp;<Checkbox
                                    onChange={this.ModalChangeIsTagActive.bind(this)}
                                    checked={this.state.editCell.content.isTagActive} />
                                &nbsp;&nbsp;@
                                <Input style={{ width: "100%" }} className="cellInput"
                                    disabled={!this.state.editCell.content.isTagActive}
                                    value={this.state.editCell.content.tag}
                                    onChange={this.ModalChangeTag.bind(this)}
                                />
                            </Col>
                        </Row>
                        <div style={{ height: "5px" }} />
                        {(this.state.editCell.type === TYPE.EQUAT) ?
                            <div>
                                <Row>
                                    <Col span={24} style={{ display: "flex", alignItems: "center" }}>
                                        <Input style={{ width: "100%" }}
                                            value={this.state.editCell.content.equat}
                                            onChange={this.ModalChangeEquation.bind(this)}
                                        />
                                        <Popover placement="bottomRight" content={colorPicker} trigger="click">
                                            <div style={styles.swatch} onClick={this.handleClickColorPicker}>
                                                <div style={styles.color} />
                                            </div>
                                        </Popover>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col span={24} style={{ display: "flex", alignItems: "center" }}>
                                        Equation : {this.state.equatTmp}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col span={24} style={{ display: "flex", alignItems: "center" }}>
                                        Résultat : {calculEquat(this.state.equatTmp)}
                                    </Col>
                                </Row>
                            </div>
                            :
                            <Row>
                                <Col span={24} style={{ display: "flex", alignItems: "center" }}>
                                    <Input style={{ width: "100%" }}
                                        value={this.state.editCell.content.value}
                                        onChange={this.ModalChangeValue.bind(this)}
                                    />
                                    <Popover placement="bottomRight" content={colorPicker} trigger="click">
                                        <div style={styles.swatch} onClick={this.handleClickColorPicker}>
                                            <div style={styles.color} />
                                        </div>
                                    </Popover>
                                </Col>
                            </Row>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Row style={{ width: '100%' }}>
                            <Col span={24} style={{ display: 'flex' }} >
                                <Button key="Remove" type="danger" onClick={this.ModalonRemoveItem.bind(this, this.state.editCell.i)} >Supprimer</Button>
                                <Button key="Save" type="primary" onClick={this.ModalSave.bind(this, this.state.editCell.i)} style={{ marginLeft: 'auto  ' }} >Enregistrer</Button>&nbsp;
                                <Button key="Cancel" onClick={this.ModalReset}>Annuler</Button>
                            </Col>
                        </Row>
                    </Modal.Footer>
                </Modal>
            </div >
        );
    }
} export default StaticGrid;