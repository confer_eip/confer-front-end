import React, { Component } from "react";
import { Image } from 'react-bootstrap';
import logo from '../../img/Logo.jpg';
import axios from 'axios';
import {message} from 'antd';
var api_url = require('../../config.json');

class Inscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      confirm_email: "",
      password: "",
      confirm_password: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {

    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  handleClick() {
    if (this.state.username === "")
      message.error("Veuillez rentrer votre prénom");
    else if (this.state.email === "")
      message.error("Veuillez rentrer votre adresse email");
    else if (this.state.confirm_email === "")
      message.error("Veuillez confirmer votre adresse email");
    else if (this.state.email !== this.state.confirm_email)
      message.error("Les adresses email sont différentes")
    else if (this.state.password === "")
      message.error("Veuillez rentrer un mot de passe");
    else if (this.state.confirm_password === "")
      message.error("Veuillez confirmer votre mot de passe");
    else if (this.state.confirm_password !== this.state.password)
      message.error("Les mots de passe sont différents");
    else {
      message.loading("Inscription en cours...");
      axios({
        method: "post",
        url: api_url.api_url + "/api/account/register",
        data: {
          "username": this.state.username,
          "email": this.state.email,
          "password": this.state.password
        }
      })
      .then(res => {
        message.destroy();
        this.props.history.push("/validateemail");
      })
      .catch(function (err) {
        message.destroy();
        if (err.response.data.username === "This username is already taken") {
          message.error("Ce pseudo est déjà pris");
        }
        else if (err.response.data.email === "Invalid email" || err.response.data.email === "This email is already taken") {
          message.error("Cet email est invalide");
        }
        else if (err.response.data.password === "This password is too simple") {
          message.error("Le mot de passe doit contenir au minimun une majuscule, une minuscule, un caractére spéciale et un chifffre et faire avoir une taille de 8");
        }
        else {
          message.error("Une erreur est survenue, veuillez réessayer");
        }
      });
    }
  }
  render() {
    return (
      <div className="wrapper fadeInDown">
        <div id="formContent">
          <div className="fadeIn first">
            <Image src={logo} id="icon" alt="User Icon" />
          </div>


          <div>
            <input type="text" id="login" className="fadeIn second" name="login" placeholder="Nom d'utilisateur" value={this.state.username} onChange={event => this.setState({ username: event.target.value })} />
            <input type="email" id="email" className="fadeIn second" name="login" placeholder="Email" value={this.state.email} onChange={event => this.setState({ email: event.target.value })} />
            <input type="email" id="verif_email" className="fadeIn second" name="login" placeholder="Vérification d'email" value={this.state.confirm_email} onChange={event => this.setState({ confirm_email: event.target.value })} />
            <input type="password" id="password" className="fadeIn third" name="login" placeholder="Mot de passe" value={this.state.password} onChange={event => this.setState({ password: event.target.value })} />
            <input type="password" id="verif_password" className="fadeIn third" name="login" placeholder="Vérification du mot de passe" value={this.state.confirm_password} onChange={event => this.setState({ confirm_password: event.target.value })} />
            <input type="submit" className="fadeIn fourth" value="Inscription" onClick={this.handleClick} />
          </div>


        </div>
        <div style={{ marginBottom: "50px" }} />
      </div>
    );
  }
}

export default Inscription;
