import React, { Component } from "react";
import axios from 'axios';
import { message } from 'antd';
import './Contact.css';
var api_url = require("../../config.json");

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      subject: "",
      message: ""
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  sendMailToConfer = () => {
    axios({
      method: "post",
      url: api_url.api_url + "/api/contact/send",
      data: {
        "name": this.state.name,
        "email": this.state.email,
        "subject": this.state.subject,
        "message": this.state.message
      }
    })
      .then(res => {
        message.success("Le mail a bien été envoyé à l'équipe. Merci :)");
      })
      .catch(function (error) {
        message.error("Echec de l'envoi du mail");
      });
  }

  render() {
    return (
      <div>
        <section className="About" name="about">
          <div className="About-us">
            <h3>À propos de nous</h3>
            <img className="Team-picture" src={require("./../../img/Confer_team.jpg")} alt="Confer team" />
            <p className="About-us-desc">Confer est un site de création et de partage de feuilles de personnages pour Jeux de rôle.
              <br />Il fut créé en 2019 dans le cadre de l'Epitech Innovative Project, projet de fin d'étude.
            </p>
          </div>
        </section>

        <section className="Contact-us" name="contact" style={{ backgroundColor: "#22427C", color: "white" }}>
          <div className="text-center">
            <div className="container">
              <div className="row">
                <div className="col-sm-6 service">
                  <h2 className="my-4">Contactez nous</h2>
                  <p className="w-responsive mx-auto mb-5">
                    Des questions ? Des suggestions ?
                    <br />Utilisez ce formulaire de contact, nous vous répondrons sous 48 heures.
                  </p>
                  <p>
                    <img className="Team-email" src={require("../../img/email.png")} alt="email" />
                    &emsp;confer_2020@epitech.eu
                  </p>
                </div>

                <div className="col-sm-6 service">
                  <div className="Form-fill">
                    <div className="Form-infos">
                      <table className="Form-name" align="center">
                        <tbody>
                          <tr>
                            <td>
                              <input className="form-control" type="text" id="name" name="name" placeholder="Nom"
                                value={this.state.name} onChange={event => this.setState({ name: event.target.value })} />
                            </td>
                            <td>
                              <input className="form-control" type="text" id="email" name="email" placeholder="Email"
                                value={this.state.email} onChange={event => this.setState({ email: event.target.value })} />
                            </td>
                          </tr>
                          <tr>
                            <td colSpan="2">
                              <input className="form-control" type="text" id="subject" name="subject" placeholder="Sujet du message"
                                value={this.state.subject} onChange={event => this.setState({ subject: event.target.value })} />
                            </td>
                          </tr>
                          <tr>
                            <td colSpan="2">
                              <textarea className="md-textarea" type="text" id="message" name="message" placeholder="Message" rows="2"
                                value={this.state.message} onChange={event => this.setState({ message: event.target.value })} >
                              </textarea>
                            </td>
                          </tr>
                          <tr>
                            <td colSpan="2">
                              <button type="submit" className="btn-contact btn btn-theme" style={{ background: "#003366", borderColor: "#2c75ff" }}
                                onClick={this.sendMailToConfer}>
                                Envoyer
                                </button>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Contact;
