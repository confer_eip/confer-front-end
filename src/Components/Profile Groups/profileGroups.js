import React, { Component } from 'react';
import './profileGroups.css';

class ProfileGroups extends Component {
  state = { isGroupLeader: true }

  unregisterGroupMember = () => {
    var elem = document.getElementById("Unregister-button");
    elem.value = "Désinscrit";
    elem.style = "background-color: #9FBDD6";
  }
  render() {
    var players = [
    "Zog Zog",
    "Le Barde"
    ]
    var playerMembers = [];
    for (var i = 0; i < players.length; i++) {
      playerMembers.push(<tr key={i}>
        <td>{players[i]}</td>
        { this.state.isGroupLeader ? <td><input type="button" id="Unregister-button" onClick={this.unregisterGroupMember} value="Désinscrire"></input></td> : null }
        </tr>
      );
    }
    return (
      <div className="ProfileGroups">
        <div className="Modal-background" onClick={this.props.onExitModal}></div>
        <div className="Modal">
          <button className="Modal-close-button" onClick={this.props.onExitModal}>×</button>
          <div className="Header">
            <h2>{this.props.group}</h2>
            <table className="Informations">
              <tbody>
                <tr>
                  <td>Leader du groupe</td>
                  <td>L'aventurier</td>
                </tr>
                <tr>
                  <td>Maître du jeu</td>
                  <td>Zangdar</td>
                </tr>
                <tr>
                  { this.state.isGroupLeader ? <td><button className="Change-roles" onClick={this.showGroupInfos}>Changer Leader</button></td> : null }
                  { this.state.isGroupLeader ? <td><button className="Change-roles" onClick={this.showGroupInfos}>Changer Chef de groupe</button></td> : null }
                </tr>
                <tr>
                  { this.state.isGroupLeader ? <td><input type="text" className="Add-player" value="Enter new member email here"></input></td> : null }
                </tr>
              </tbody>
            </table>
          </div>
          <table className="Players">
            <thead>
              <tr>
                <th>Pseudonyme</th>
                { this.state.isGroupLeader ? <th>Status</th> : null }
              </tr>
            </thead>
            <tbody>
              {playerMembers}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ProfileGroups;
