import React from "react";
import './Footer.css';
import { Row, Col, Container } from 'react-bootstrap';
import { NavLink, HashRouter } from "react-router-dom";

const Footer = () => {

    return (
        <HashRouter>
            <footer className="footer" >
                <Container>
                    <Row>
                        <Col md={{ span: 3 }} lg={{ span: 3 }} sm={{ span: 3 }}>
                            <h1 className="title">Confer</h1>
                        </Col>
                        <Col md={{ span: 6 }} lg={{ span: 6 }} sm={{ span: 6 }} className="h-100 text-center text-lg-left my-auto">
                            <ul className="list-inline mb-2">
                                <li className="list-inline-item">
                                    <NavLink to="/contact">Contact</NavLink>
                                </li>
                                <li className="list-inline-item"></li>
                                <li className="list-inline-item">
                                    <a href="/">Condition d'utilisation</a>
                                </li>
                                <li className="list-inline-item"></li>
                                <li className="list-inline-item">
                                    <a href="/">Politique de confidentialité</a>
                                </li>
                            </ul>
                            <p className="text-muted small mb-4 mb-lg-0 all-right">Confer 2019. All Rights Reserved.</p>
                        </Col>
                    </Row>
                </Container>
            </footer>
        </HashRouter>
    );
}

export default Footer;
