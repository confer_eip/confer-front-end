import React, { Component } from "react";
import { CardDeck, Card, Button } from 'react-bootstrap';
import axios from 'axios';
import { message } from 'antd';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('../../config.json');

class GroupSheets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sheets: [{
        "sheet_name": "",
        "sheet_id": "",
        "sheet_description": "",
        "sheet_data": ""
      }],
      empty_sheets: true
    };

    this.getGroupSheets();
  }

  getGroupSheets() {
    var token = cookies.get('LoginToken');
    const group_id = window.location.href.split("/");
    axios({
      method: "post",
      headers: { 'Content-Type': 'application/json' },
      url: api_url.api_url + "/api/group/get_sheets",
      data: {
        "jwt": token.jwt,
        "group_id": parseInt(group_id[group_id.length - 1], 10)
      }
    })
      .then(({ data }) => {
        this.setState({ sheets: data.sheets });
        if (data.sheets.length !== 0)
          this.setState({ empty_sheets: false });
        return (data.data);
      })
      .catch(function (error) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
  }

  goBackToGroups = () => {
    this.props.history.push("/groups");
  }

  previewTemplate(data) {
    data.isFromGalerie = true;
    this.props.history.push({
      pathname: "/personnage",
      data: data,
    });
  }

  render() {
    return (
      <div style={{ margin: "0 10%" }}>
        <Button variant="primary" style={{ marginBottom: "30px" }} onClick={this.goBackToGroups}>↺ Retour</Button>
        <h1 style={{ textAlign: 'center', marginBottom: '40px' }}>Feuilles du groupe</h1>
        {this.state.empty_sheets === true
          ? <p>Ce groupe n'a aucune feuille de personnages.</p>
          :
          <CardDeck>
            {this.state.sheets.map(sheet => (
              <Card key={sheet.sheet_id} text="white" style={{ minWidth: "16rem", marginBottom: "40px" }}>
                <Card.Header style={{ background: "linear-gradient(#1e1e2f,#1e1e24)" }}>
                  {sheet.sheet_name.toUpperCase()}
                </Card.Header>
                <Card.Body style={{ backgroundColor: "#318CE7" }}>
                  ID : {sheet.sheet_id}
                  <br />                  <br />
                  {sheet.sheet_description}
                  <br />                  <br />
                  <Button variant="secondary" onClick={() => this.previewTemplate(sheet)}>
                    Preview
                    </Button>
                </Card.Body>
              </Card>
            ))}
          </CardDeck>
        }
      </div>
    );
  }
}

export default GroupSheets;