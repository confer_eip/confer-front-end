import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Image } from 'react-bootstrap';
import axios from 'axios';
import { message } from 'antd';
var api_url = require('../../config.json');

class ForgottenPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ""
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  handleClick() {
    if (this.state.email === "")
      message.error("Veuillez rentrer une adresse email")
    else {
      axios({
        method: "post",
        url: api_url.api_url + "/api/account/reset_password",
        data: {"email": this.state.email}
      })
      .then(res => {
        message.success("Un mail contenant le nouveau mot de passe vous a été envoyé");
        this.props.history.push("/connexion");
      })
      .catch(function (err) {
        message.error("Une erreur est survenue, veuillez réessayer");
      });
    }
  }
  render() {
    return (
      <div className="wrapper fadeInDown">
        <div id="formContent">
          <div className="fadeIn first">
          <Image src={require('../../img/Logo.jpg')} id="icon" alt="User Icon" />
          </div>
          <div>
            <input type="text" id="mail" className="fadeIn second" name="mail" placeholder="mail" value={this.state.email} onChange={event => this.setState({email:event.target.value})}/>
            <input type="submit" className="fadeIn fourth" value="Réinitialiser mon mot de passe" onClick={this.handleClick}/>
          </div>
          <div id="formFooter">
            <NavLink to="/connexion"><a className="underlineHover" href="/">Retour à la page de connexion</a></NavLink>
          </div>
        </div>
      </div>
    );
  }
}

export default ForgottenPassword;