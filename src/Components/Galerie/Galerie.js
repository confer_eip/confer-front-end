import React, { Component } from "react";
import { CardDeck, Card, DropdownButton, Dropdown, Button } from 'react-bootstrap';
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import axios from 'axios';
import ModalBlockGallery from './../../modals/ModalBlockGallery';
import Cookies from 'universal-cookie';
export const cookies = new Cookies();
var api_url = require('../../config.json');

const MODAL = {
  NONE: 'none',
  BLOCK: 'block',
}

class Galerie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current_page: 0,
      order_templates: "views",
      sheets: [],
      showModal: MODAL.NONE,
      sheet_infos: null
    };

    this.getTemplates(this.state.current_page);
    this.handleChange = this.handleChange;
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  getTemplates = (page) => {
    axios({
      method: "post",
      headers: { 'Content-Type': 'application/json' },
      url: api_url.api_url + "/api/gallery/",
      data: {
        page: page,
        order_method: this.state.order_templates
      }
    })
      .then(({ data }) => {
        if (data.templates.length > 0) {
          this.setState({
            sheets: data.templates,
            current_page: page
          })
        }
        return (data.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  }

  getTemplatesSetOrderByVotesAsc = () => {
    this.setState({ order_templates: "votes", current_page: 0 },
      () => { this.getTemplates(this.state.current_page) });
  }

  getTemplatesSetOrderByVotesDesc = () => {
    this.setState({ order_templates: "-votes", current_page: 0 },
      () => { this.getTemplates(this.state.current_page) });
  }

  getTemplatesSetOrderByViewsAsc = () => {
    this.setState({ order_templates: "views", current_page: 0 },
      () => { this.getTemplates(this.state.current_page) });
  }

  getTemplatesSetOrderByViewsDesc = () => {
    this.setState({ order_templates: "-views", current_page: 0 },
      () => { this.getTemplates(this.state.current_page) });
  }

  getPreviousTemplatesPage = () => {
    if (this.state.current_page > 0) {
      this.getTemplates(this.state.current_page - 1);
    };
  }

  getNextTemplatesPage = () => {
    this.getTemplates(this.state.current_page + 1);
  }

  showBlockGallery(sheet) {
    var date = sheet.publication_date;
    date = `${date.slice(0, 10)} ${date.slice(11, 16)}`;
    sheet.publication_date = date;
    this.setState({ showModal: MODAL.BLOCK, sheet_infos: sheet });
  }

  exitBlockGallery = () => {
    this.setState({ showModal: MODAL.NONE });
  }

  handleRedirect = (data) => {
    this.props.history.push(data);
  }

  render() {
    return (
      <div style={{ margin: "0 10%" }}>
        <h1 style={{ textAlign: 'center' }}>Galerie</h1>
        <h2 style={{ textAlign: 'center' }}>de modèles de feuilles de personnages</h2>

        <DropdownButton id="dropdown-basic-button" title="Trier par" style={{ textAlign: "right", marginBottom: "10px" }}>
          <Dropdown.Item onClick={this.getTemplatesSetOrderByVotesAsc}>Votes : par ordre croissant</Dropdown.Item>
          <Dropdown.Item onClick={this.getTemplatesSetOrderByVotesDesc}>Votes : par ordre décroissant</Dropdown.Item>
          <Dropdown.Item onClick={this.getTemplatesSetOrderByViewsAsc}>Vues : par ordre croissant</Dropdown.Item>
          <Dropdown.Item onClick={this.getTemplatesSetOrderByViewsDesc}>Vues : par ordre décroissant</Dropdown.Item>
        </DropdownButton>

        <CardDeck>
          {this.state.sheets.map(sheet => (
            <Card key={sheet.template_id} onClick={this.showBlockGallery.bind(this, sheet)}
              text="white" style={{ minWidth: "16rem", marginBottom: "40px", cursor: "pointer" }} >
              <Card.Header style={{ backgroundColor: "#252525" }}>{sheet.template_name.toUpperCase()}</Card.Header>
              <Card.Body style={{ backgroundColor: "#318CE7" }} >
                <Rater total={5} rating={sheet.vote_average} interactive={false} />
                <Card.Text>
                  {sheet.template_description}
                </Card.Text>
              </Card.Body>
              <Card.Footer style={{ backgroundColor: "#252525" }} >
                <small>Auteur : {sheet.owner_name}</small>
              </Card.Footer>
            </Card>
          ))}
        </CardDeck>

        <nav aria-label="Page" style={{ marginBottom: "80px" }}>
          <ul className="pagination justify-content-center">
            <li className="page-item" style={{ width: "100px" }}>
              <Button variant="primary" onClick={this.getPreviousTemplatesPage}>Precedent</Button>
            </li>
            <li className="page-item" style={{ width: "100px" }}>
              <Button variant="primary" onClick={this.getNextTemplatesPage}>Suivant</Button>
            </li>
          </ul>
        </nav>

        {this.state.showModal === MODAL.BLOCK
          ? <ModalBlockGallery onExitModal={this.exitBlockGallery} onShowModal={true} sheet={this.state.sheet_infos} redirect={this.handleRedirect} /> : null
        }
        <div style={{ marginBottom: "100px" }} />
      </div>
    );
  }
}

export default Galerie;
