import React, { Component } from "react";
import { Route, NavLink, HashRouter } from "react-router-dom";
import { Container } from 'react-bootstrap';
import Home from "../../Home";
import axios from 'axios';
import Galerie from "../Galerie/Galerie";
import Connexion from "../Connexion/Connexion";
import Inscription from "../Inscription/Inscription";
import Contact from "../Contact/Contact";
import Annonces from "../Annonces/Annonces";
import LandingPage from "../LandingPage/LandingPage";
import ForgottenPassword from "../Forgotten Password/ForgottenPassword";
import ValidateEmail from "../Validate Email/ValidateEmail";
import StaticGrid from '../StaticGrid/StaticGrid';
import GroupSheets from '../GroupSheets/GroupSheets';
import Groups from "../Groups/Groups";
import Cookies from 'universal-cookie';
import "./Header.css"
export const cookies = new Cookies();
var api_url = require('../../config.json');

export const IS_CONNECTED = {
  CONNECTED: 'connected',
  NOT_CONNECTED: 'not_connected',
};

class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      logged: this.getSource()
    };

    this.handleLogOut = this.handleLogOut.bind(this);
  }

  handleLogOut() {
    axios({
      method: "post",
      url: api_url.api_url + "/api/account/logout",
      data: cookies.get('LoginToken')
    })
      .then(res => {
        cookies.remove("LoginToken");
        cookies.remove('Sheetdata');
        window.location.reload();
        this.props.history.push("/connexion");
      })
      .catch(function (error) {
        cookies.remove("LoginToken");
      });
  }

  getSource() {
    if (cookies.get('LoginToken')) {
      return IS_CONNECTED.CONNECTED;
    }
    return IS_CONNECTED.NOT_CONNECTED;
  }

  render() {
    return (
      <HashRouter>
        <header id="header">
          <Container className="test">
            <div id="logo" className="pull-left">
              <h1><NavLink exact to="/" className="title-header">Confer</NavLink></h1>
            </div>
            <div className="nav-menu-container pull-right redirect-page">
              <div>
                {this.state.logged === IS_CONNECTED.NOT_CONNECTED ? (
                  <ul className="nav-menu">
                    <li><NavLink exact to="/" >Accueil</NavLink></li>
                    <li><NavLink to="/galerie">Galerie</NavLink></li>
                    <li><NavLink to="/contact">Contact</NavLink></li>
                    <li><NavLink to="/connexion">Connexion</NavLink></li>
                    <li><NavLink to="/inscription">Inscription</NavLink></li>
                  </ul>
                ) : (
                    <ul className="nav-menu">
                      <li><NavLink to="/home">Mon Profil</NavLink></li>
                      <li><NavLink to="/groups">Mes groupes</NavLink></li>
                      <li><NavLink to="/galerie">Galerie</NavLink></li>
                      <li><NavLink to="/annonces">Annonces</NavLink></li>
                      <li><NavLink to="/contact">Contact</NavLink></li>
                      <li><button onClick={this.handleLogOut} style={{ backgroundColor: "#252525", borderStyle: "none" }}><NavLink to="/">Déconnexion</NavLink></button></li>
                    </ul>
                  )}
              </div>

            </div>
          </Container>
        </header >
        <div className="content" style={{ marginTop: '120px' }}>
          <Route exact path="/" component={LandingPage} />
          <Route path="/home" component={Home} />
          <Route path="/galerie" component={Galerie} />
          <Route path="/inscription" component={Inscription} />
          <Route path="/connexion" component={Connexion} />
          <Route path="/contact" component={Contact} />
          <Route path="/annonces" component={Annonces} />
          <Route path="/personnage" component={StaticGrid} />
          <Route path="/landingpage" component={LandingPage} />
          <Route path="/forgottenpwd" component={ForgottenPassword} />
          <Route path="/validateemail" component={ValidateEmail} />
          <Route path="/groupsheets/:id_group" component={GroupSheets} />
          <Route path="/groups" component={Groups} />
        </div>
      </HashRouter>
    );
  }
}

export default Header;
