import React, { Component } from "react";
import { Card, Row, Col, Container, Button, Image } from 'react-bootstrap';
import axios from 'axios';
import { message } from 'antd';
import ModalRemoveAccount from './modals/Profile/ModalRemoveAccount';
import ModalChangePassword from './modals/Profile/ModalChangePassword';
import ModalSheet from './modals/Profile/ModalSheet';
import Cookies from 'universal-cookie';
import FileBase64 from 'react-file-base64';
export const cookies = new Cookies();

var api_url = require('./config.json');

const MODAL = {
  NONE: 'none',
  SHEET: 'sheet',
  REMOVE: 'remove',
  PWD_CHANGE: 'pwd_change',
}

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      user_id: '',
      sheets: [],
      showModal: MODAL.NONE,
      sheet_content: [],
      files: '',
      image: ''
    };

    this.RequestProfile();
    this.handleLogOut = this.handleLogOut.bind(this);
  }

  RequestProfile() {
    if (cookies.get('refresh') === 'true') {
      cookies.set('refresh', 'false');
      window.location.reload();
    }
    if (cookies.get('LoginToken')) {
      const cookie = cookies.get('LoginToken');
      axios({
        method: "post",
        url: api_url.api_url + "/api/account/profile",
        data: cookie
      })
        .then(({ data }) => {
          this.setState({
            username: data.username,
            email: data.email,
            groups: data.groups,
            user_id: data.user_id,
            invitations: data.invitations,
            sheets: data.sheets,
            image: data.image
          })
          axios({
            method: "post",
            url: api_url.api_url + "/api/sheet/list",
            data: {
              "jwt": cookie.jwt,
              "sheets_id": this.state.sheets
            }
          }).then(({ data }) => {
            this.setState({
              sheet_content: data.sheets
            })
          })
        })
        .catch((error) => {
          console.error(error)
        });
    }
    else {
      this.props.history.push("/");
    }
  }

  changeImage(file) {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/account/change_image",
      data: {
        "jwt": token.jwt,
        "image_format": "png",
        "image_data": file.base64.replace("data:image/png;base64,", "")
      }
    })
      .then(res => {
        this.RequestProfile();
      })
      .catch(function (error) {
        message.error("Erreur lors de chargement du fichier. Est-ce bien une image ?");
      });
  }

  getFiles(files) {
    this.setState({ files: files });
    this.changeImage(files);
  }

  showRemoveUser = () => {
    this.setState({ showModal: MODAL.REMOVE });
  }
  exitRemoveUser = () => {
    this.setState({ showModal: MODAL.NONE });
    this.RequestProfile();
  }

  showSheet = () => {
    this.setState({ showModal: MODAL.SHEET });
  }

  exitSheet = () => {
    this.setState({ showModal: MODAL.NONE });
    this.RequestProfile();
  }

  showChangePassword = () => {
    this.setState({ showModal: MODAL.PWD_CHANGE });
  }

  exitChangePassword = () => {
    this.setState({ showModal: MODAL.NONE });
  }

  deleteSheet(sheet_id) {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/sheet/remove",
      data: {
        "sheet_id": sheet_id,
        "jwt": token.jwt
      }
    })
      .then(res => {
        this.RequestProfile();
      })
      .catch(function (error) {
        console.error(error);
      });
  }

  getSheetData(sheet_id) {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/sheet/get",
      data: {
        "sheet_id": sheet_id,
        "jwt": token.jwt
      }
    })
      .then(res => {
        res.data.mode = "edit";
        this.props.history.push({
          pathname: "/personnage",
          data: res.data
        });
      })
      .catch(function (error) {
        console.error(error);
      });
  }

  playSheetData(sheet_id) {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/sheet/get",
      data: {
        "sheet_id": sheet_id,
        "jwt": token.jwt
      }
    })
      .then(res => {
        res.data.mode = "play";
        this.props.history.push({
          pathname: "/personnage",
          data: res.data
        });
      })
      .catch(function (error) {
        console.error(error);
      });
  }

  addTemplate(sheet_id) {
    var token = cookies.get('LoginToken');
    axios({
      method: "post",
      url: api_url.api_url + "/api/gallery/share",
      data: {
        "sheet_id": sheet_id,
        "jwt": token.jwt
      }
    })
      .then(res => {
        message.success("La feuille a bien été ajouté à la galerie");
        this.props.history.push("/home");
      })
      .catch(function (error) {
        message.error("La feuille n'a pas pû être rajouté à la galerie");
      });
  }

  handleLogOut() {
    axios({
      method: "post",
      url: api_url.api_url + "/api/account/logout",
      data: cookies.get('LoginToken')
    })
      .then(res => {
        cookies.remove("LoginToken");
        cookies.remove('Sheetdata');
        cookies.set('refresh', 'true');

        this.props.history.push("/connexion");
      })
      .catch(function (error) {
        cookies.remove("LoginToken");
        cookies.remove('Sheetdata');
      });
  }

  render() {
    return (
      <Container>
        <Row className="margin_bottom_50px">
          <Col md={{ span: 6 }}>
            <Row>
              <Col md={{ span: 6 }}>
                <Image className="user-image" src={this.state.image} rounded />
              </Col>
              <Col md={{ span: 6 }}>
                <br />
                <FileBase64 id="file-upload" type="file" multiple={false} onDone={this.getFiles.bind(this)} />
                <label className="label-file">Modifier image de profil</label>
              </Col>
            </Row>
          </Col>
          <Col md={{ span: 6 }}>
            <Row>
              <Col md={{ span: 12, offset: 0 }}
                style={{ fontFamily: "Trebuchet MS, sans-serif", fontSize: "20px", textAlign: "left" }}>
                <b>ID:</b> {this.state.user_id}
              </Col>
              <Col md={{ span: 12, offset: 0 }}
                style={{ fontFamily: "Trebuchet MS, sans-serif", fontSize: "20px", textAlign: "left" }}>
                <b>Pseudo:</b> {this.state.username}
              </Col>
              <Col md={{ span: 12, offset: 0 }}
                style={{ fontFamily: "Trebuchet MS, sans-serif", fontSize: "20px", textAlign: "left" }}>
                <b>Email:</b> {this.state.email}
              </Col>
              <Col style={{ textAlign: "left" }}>
                <Button variant="primary" className="Button-space-small-screen" style={{ marginRight: "15px" }}
                  onClick={this.showChangePassword}>
                  Changer le mot de passe
                </Button>
                <Button variant="danger" onClick={this.showRemoveUser}>
                  Supprimer le compte
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>

        <h1 style={{ textAlign: 'center', marginBottom: '40px' }}>Mes feuilles</h1>
        <Container style={{ fontFamily: "Trebuchet MS, sans-serif", fontSize: "20px" }}>
          {this.state.sheet_content.map(sheet => (
            <Card key={sheet.sheet_id} className="border_solid">
              <Card.Header style={{ backgroundColor: "white" }}>
                <Row>
                  <Col md={{ span: 2, offset: 0 }}>
                    ID: {sheet.sheet_id}
                  </Col>
                  <Col md={{ span: 8, offset: 0 }}>
                    {sheet.sheet_name}
                  </Col>
                </Row>
              </Card.Header>
              <Card.Body>
                <Row style={{ textAlign: 'center' }}>
                  <Col>
                    <Button variant="primary" className="Button-space-small-screen"
                      onClick={this.playSheetData.bind(this, sheet.sheet_id)}>
                      ▶ Jouer
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="primary" className="Button-space-small-screen"
                      onClick={this.getSheetData.bind(this, sheet.sheet_id)}>
                      Modifier
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="danger" className="Button-space-small-screen"
                      onClick={this.deleteSheet.bind(this, sheet.sheet_id)}>
                      Supprimer
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="primary"
                      onClick={this.addTemplate.bind(this, sheet.sheet_id)}>
                      Ajouter Galerie
                    </Button>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          ))}
          <Card className="border_solid height_60px" style={{ marginBottom: '100px' }}>
            <Col className="align_center">
              <Button variant="outline-dark" name="Créer une feuille" onClick={this.showSheet}>+</Button>
            </Col>
          </Card>
        </Container>

        {this.state.showModal === MODAL.REMOVE
          ? <ModalRemoveAccount onExitModal={this.exitRemoveUser} onShowModal={true} username={this.state.username} changePage={() => this.props.history.push("/connexion")} /> : null}
        {this.state.showModal === MODAL.PWD_CHANGE
          ? <ModalChangePassword onExitModal={this.exitChangePassword} onShowModal={true} username={this.state.username} /> : null}
        {this.state.showModal === MODAL.SHEET
          ? <ModalSheet onExitModal={this.exitSheet} onShowModal={true} /> : null}
      </Container>
    );
  }
}

export default Home;
