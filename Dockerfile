FROM node:8.10.0

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY . .
RUN npm install --silent

CMD ["npm","start"]

